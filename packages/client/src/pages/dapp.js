import DappLib from "@decentology/dappstarter-dapplib";
import DOM from "../components/dom";
import "../components/action-card.js";
import "../components/action-button.js";
import "../components/text-widget.js";
import "../components/number-widget.js";
import "../components/account-widget.js";
import "../components/upload-widget.js";
import { unsafeHTML } from "lit-html/directives/unsafe-html";
import { LitElement, html, customElement, property } from "lit-element";

@customElement("dapp-page")
export default class DappPage extends LitElement {
  @property()
  get;
  @property()
  post;
  @property()
  title;
  @property()
  category;
  @property()
  description;

  createRenderRoot() {
    return this;
  }
  constructor(args) {
    super(args);
  }


  render() {
    let content = html`
      <div class="container m-auto">
        <div class="row fadeIn mt-3 p-2 block">
          <h2 class="text-5xl">Patient Data NFT Marketplace</h2>
          <p class="mt-3">
            <i>
              Reward and protect patient data via NFT royalties and multi-signature!
            </i>
          </p>
          <br/>
          <p class="mt-3">
            Patient data is collected with every visit to the hospital.  However, patient consent to collection and sharing needs to be explicit, and the patient is rarely, if ever, rewarded for sharing personal data.
            <br/>
            <br/>
            Team PHDDI has designed a workflow to link patient data to NFT minting, and deliberately seeks patient consent when transferring the NFT to Third Parties (e.g. Pharma) via multi-signature, as well as hard-coding a royalty rate into the smart contract so that the patient will be rewarded at the initial and every subsequent sale of that NFT.
          </p>
          <br/>
          <br/>
          <img src="https://gitlab.com/phddi/2021-mercury-hackathon-patient-consent/-/raw/master/packages/client/src/assets/case-scenario.png" alt='logo-case_scenario' />
          <br/>
          <h3 class="mt-3 text-3xl">Next Steps</h3>
          <ul class="mt-3 ml-5 list-decimal">
            <li class="mt-3">Setup: Tenancy and Provision Accounts.</li>
            <li class="mt-3">Hospital Portal: Mint and List NFT.</li>
            <li class="mt-3">Patient Portal: Get NFT metadata and sell NFT with multi-signature.</li>
            <li class="mt-3">Pharma Portal: Provision account and buy NFT.</i></li>
          </ul>
        </div>
      </div>
    `;
    return content;

  }
}
